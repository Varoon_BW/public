# -*- coding: utf-8 -*-
"""
Created on Thu Dec 08 12:55:21 2016

@author: Varoon
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Dec 05 19:47:42 2016

@author: Varoon
"""


from time import strftime
from time import sleep
import logging
import boto3


SQL_INSTANCE_ID = 'i-028589fe315eeeeac'
#FIL_INSTANCE_ID = 'i-02356c03fd844b274'
#PGP_INSTANCE_ID = 'i-0cd92a451929228ea'
#BUCKET_NAME = 'ami-creation-logs'
#FILE_NAME = 'create_ami_log1_' + strftime("%Y%m%d") + '.log'

#logging.basicConfig(filename=FILE_NAME,
#                    filemode='w',
#                    level=logging.INFO,
#                    format='%(asctime)s %(message)s'
#                   )


LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)



def get_connection(session):
    """
    Method creates connection with user profile specified and returns
    ec2 resource reference.
    """

    ec2_resource = session.resource('ec2')
    return ec2_resource


def get_name_from_tags(tags):
    """
    Method accepts tags attribute of an instance , iterates over the list
    of dicts of tags and returns the string associated with the Name tag.

    Accepts : tags attribute of an instance (list of dicts)

    Returns : string
    """
    name = 'NoName'

    for tag in tags:
        if tag['Key'] == 'Name':
            name = tag['Value']
    return name


def get_app_id_from_tags(tags):
    """
    Method accepts tags attribute of an instance , iterates over the list
    of dicts of tags and returns the string associated with the App-ID tag.

    Accepts : tags attribute of an instance (list of dicts)

    Returns : string
    """
    app_id = 'No_App_Id'
    for tag in tags:
        if tag['Key'] == 'ApplicationId':
            app_id = tag['Value']
    return app_id


def create_ami(instance, name, app_id):
    """
    Method creates an AMI for the instance whose reference is passeds
    as an argument

    Accepts: ec2 instance reference , Name tag of the instance , App-ID tag
    of the instance

    Returns: returns Image reference for the AMI created.
    """
    time_str = strftime("%Y%m%d")
    LOGGER.info('*****creating AMI for %s*****', name)
    image = instance.create_image(
        Name='{0} {1}'.format(name, time_str),
        Description='AMI created for instance {0} on {1}'.format(
            name, time_str))

    while not image.state == 'available':
        LOGGER.info('creating...')
        sleep(15)
        image.load()

    LOGGER.info('AMI for %s created', name)
    LOGGER.info('Tagging AMI for %s', name)
    image.create_tags(Tags=[
        {'Key': 'Name', 'Value': '{0} {1}'.format(name, time_str)},
        {'Key': 'ApplicationId', 'Value': app_id}
        ])

#    print 'Image created {0}'.format(image.id)
    LOGGER.info('Tagging complete')
    return image


def call_create_ami(ec2, instance_ids):
    """
    Method Iterates over list of instance ids and for each instance calls
    1.Create_AMI
    2.get_snapshot_id_from_block_device which in turn calls tag_snapshot
      which tags snapshots associated with the AMI.
    """

    for i_id in instance_ids:
        instance = ec2.Instance(i_id)

        name = get_name_from_tags(instance.tags)
        if 'sql' in name.lower():
            name = 'AWSFSFSQL01V'
        if 'pgp' in name.lower():
            name = 'AWSFSFPGP01V'
        if 'terminal' in name.lower():
            name = 'AWSFSFTRL01V'
        if 'file' in name.lower():
            name = 'AWSFSFFIL01V'

        app_id = get_app_id_from_tags(instance.tags)
        image = create_ami(instance, name, app_id)
        get_snapshot_id_from_block_device(image.block_device_mappings,
                                          ec2, name, app_id)
    return image


def tag_snapshot(ec2, snap_id, name, app_id):
    """
    Method tags snapshots associated with the AMI

    Accepts: ec2 resource reference, AMI id , SnapShot id passed from the
    method get_snapshot_id_from_block_device , name of the instance , app_id
    of the instance

    Returns: void
    """

    snapshot = ec2.Snapshot(snap_id)
    time_str = strftime("%Y%m%d")
    snapshot.create_tags(Tags=[
        {'Key': 'Name', 'Value': '{0} {1}'.format(name, time_str)},
        {'Key': 'ApplicationId', 'Value': app_id}
        ])


def get_snapshot_id_from_block_device(block_device, ec2, name, app_id):
    """
    Method iterates over block device mappings and for each device(dict),
    retrieves the SnapshotId and calls tag_snapshot method

    Accepts: Block Device Mappings , ec2 resource reference, AMI id,s
    SnapShot id passed from the method get_snapshot_id_from_block_device,
    name of the instance, app_id of the instance

    Returns: void
    """

    for device in block_device:
        LOGGER.info('Tagging snapshot of AMI %s', name)
        snap_id = device['Ebs']['SnapshotId']
        tag_snapshot(ec2, snap_id, name, app_id)
    LOGGER.info('Tagging snapshot complete')


#def upload_log_file_to_s3(session):
#    """
#
#    Method uploads create_ami_log file to S3
#
#    """
#    s3_resource = session.resource('s3')
#    bucket = s3_resource.Bucket(BUCKET_NAME)
#    bucket.upload_file(FILE_NAME, FILE_NAME)
#    LOGGER.info('log file uploaded to s3')


def my_lambda_func(event,context):
    '''
        Main function to create amis of instancess
    '''
    LOGGER.info('In program main()')
    session = boto3.Session()
    ec2_resource = get_connection(session)
    instance_ids = [SQL_INSTANCE_ID]
    image = call_create_ami(ec2_resource, instance_ids)
    LOGGER.info( 'Image {0} created'.format(image.id))
    LOGGER.info('AMI creation for instances complete')
    #upload_log_file_to_s3(session)
    return {'message':'Finally!!!'}
