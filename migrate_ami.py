# -*- coding: utf-8 -*-
"""
Created on Sat Sep 17 15:53:35 2016

@author: Varoon Ravi

Purpose: Script to migrate an instance with encrypted EBS volumes
         across accounts and across regions.
"""

import time
import logging
import argparse
from copy import deepcopy

import boto3


def get_name_from_tags(list_tagpair_dicts):
    """
        This function returns the value of the 'Name' tag if exists

        Arguments: List of dictionaries.
               Example: [{'Name': 'XYZ Test Instance'},{'Environment': 'Dev}]

        Output: String
    """
    if list_tagpair_dicts:
        for tag_pair in list_tagpair_dicts:
            try:
                if tag_pair['Key'] == 'Name':
                    return tag_pair['Value']
            except KeyError:
                continue
    return 'NoName'

def get_app_id_from_tags(list_tagpair_dicts):
    """
    This function retrns the value of the 'Application-ID' tag if exists

    Argumnets:List of dictionaries of all tags associated with the instance
              Example: [{'Name': 'XYZ Test Instance'},{'Environment': 'Dev}]

    Returns: String
    """
    if list_tagpair_dicts:
        for tag_pair in list_tagpair_dicts:
            try:
                if tag_pair['Key'] == 'Application-ID':
                    return tag_pair['Value']
            except KeyError:
                continue
        return 'No ApplicationID'


def create_connection_ec2(region, account_profile):
    """
        Create a ec2 resource service client in the given AWS
        region for the given AWS profile

        Output: ec2 connection resource
    """
    session = boto3.session.Session(
        region_name=region,
        profile_name=account_profile)
    ec2 = session.resource('ec2')
    return ec2


def create_image(ec2, instance_id, app_id):
    """
        Create an image of the EC2 instance.
        This function will reboot the server to get a clean copy

        Output: Image
    """
    logging.info('Creating AMI of instance-id: %s\n', instance_id)

    instance = ec2.Instance(instance_id)
    instance_name = get_name_from_tags(instance.tags)

    image = instance.create_image(
        Name='AMI for instanceId {0} - {1}'.format(
            instance_id, instance_name),
        Description='AMI for instanceId {0} - {1}'.format(
            instance_id, instance_name))
    image.create_tags(Tags=[
    {'Key':'Name',
     'Value':'AMI for instanceId {0} - {1}'.format(
     instance_id, instance_name)
    },
    {'Key':'Application-ID',
     'Value':app_id}
    ])

#    logging.info('Waiting for AMI %s to be available', image.id)
#
#    waiter = ec2.get_waiter('image_available')
#    waiter.wait(ImageIds=[image.id])

    while not image.state == 'available':
        logging.info('Waiting for AMI %s to be available', image.id)
        image.load()
        time.sleep(10)

    logging.info('AMI %s has been created for instance-id: %s\n',
                 image.id, instance_id)

    return image


def copy_snapshot_from_source_dest(ec2, snapshot_id, customer_managed_key,instance_name, app_id):
    """
        Create a copy of the encrypted snapshot from the source region to the
        destination region. The copy is encrypted using a shared customer
        managed key. This key has have to be shared with the receiving account.

        Output: Snapshot
    """
    

    logging.info('Copying snapshot id %s from source to destination\n',
                 snapshot_id)
    snapshot = ec2.Snapshot(snapshot_id)
    copy_snapshot_response = snapshot.copy(
        Description='Copy of {0} from {1}'.format(
            snapshot.snapshot_id, args.source_region),
        SourceRegion=args.source_region,
        DestinationRegion=args.destination_region,
        Encrypted=True,
        KmsKeyId=customer_managed_key)
    copied_snapshot_id = copy_snapshot_response['SnapshotId']
    copied_snapshot = ec2.Snapshot(copied_snapshot_id)
 
    copied_snapshot.create_tags(Tags=[
    {'Key':'Name',
     'Value':'Snapshot for volume of instance {0}'.format(instance_name)
    },
    {'Key':'Application-Id',
     'Value':app_id
    }
    ])

    # Wait until the snapshot copying is completed.
    while not copied_snapshot.state == 'completed':
        logging.info('Waiting for snaptshot copy %s to be completed',
                     copied_snapshot_id)
        copied_snapshot.load()
        time.sleep(30)
#    logging.info('Waiting for snaptshot copy %s to be completed',
#                 copied_snapshot_id)

#    waiter = ec2.get_waiter('snapshot_completed')
#    waiter.wait(SnapshotIds=[copied_snapshot_id])

#    copied_snapshot.wait_until_completed(
#        Filters=[{'Name': 'snapshot_id',
#                  'Values': [copied_snapshot_id]}])

    logging.info('Snapshot %s has been copied to new snapshot: %s\n',
                 snapshot_id, copied_snapshot_id)

    return copied_snapshot


def grant_access_to_receiver_account(snapshot, receiving_accountids):
    """
        Modify snapshot attributes to allow the snapshot to be read by the
        receiving account.

        Output: None
    """

    logging.info('Granting access to desitnation account to access %s\n',
                 snapshot.id)

    snapshot.modify_attribute(Attribute='createVolumePermission',
                              OperationType='add',
                              UserIds=receiving_accountids)

    return


def copy_snapshot_default_cmk(ec2, snapshot_id):
    """
        Create a copy of the encrypted snapshot within same region using the
        default CMK (customer managed key).

        Output: Snapshot
    """

    logging.info('Copying snapshot id %s using default CMK\n',
                 snapshot_id)
    snapshot = ec2.Snapshot(snapshot_id)
    copy_snapshot_response = snapshot.copy(
        Description='Owned copy of shared snapshot {0}'
        .format(snapshot_id),
        SourceRegion=args.destination_region)
    copied_snapshot_id = copy_snapshot_response['SnapshotId']
    copied_snapshot = ec2.Snapshot(copied_snapshot_id)

    # Wait until the snapshot copying is completed.
    while not copied_snapshot.state == 'completed':
        logging.info('Waiting for snaptshot copy %s to be completed',
                     copied_snapshot_id)
        copied_snapshot.load()
        time.sleep(30)

#    logging.info('Waiting for snaptshot copy %s to be completed',
#                 copied_snapshot_id)

#    copied_snapshot.wait_until_completed(
#        Filters=[{'Name': 'snapshot_id',
#                  'Values': [copied_snapshot_id]}])

#    waiter = ec2.get_waiter('snapshot_completed')
#    waiter.wait(SnapshotIds=[copied_snapshot_id])

    logging.info('Snapshot %s has been copied to new snapshot: %s\n',
                 snapshot_id, copied_snapshot_id)

    return copied_snapshot


def get_base_ami(region_name, operating_system='win2012R2'):
    if operating_system != 'win2012R2':
        logging.error('Migration script does not handle this OS.')

    ami_lookup = {'us-east-1': 'ami-ee7805f9',
                  'us-west-2': 'ami-2827f548', # 'ami-1712d877',
                  'ap-southeast-1': 'ami-d6f32ab5'}

    return ami_lookup[region_name]


def get_key_name(region_name):
    
    """
    This function checks if region_name(string) is available in the and does
    a look up for the corresponding associated key pair
    
    Accepts retion_name (string)
    
    Returns key_pair (string)
    """
    if region_name not in ('us-east-1', 'us-west-2', 'ap-southeast-1'):
        logging.error('Migration script does not handle this destination'
                      ' region.')

    key_lookup = {'us-east-1': 'DG_NViriginia',
                  'us-west-2': 'DG_Oregon',
                  'ap-southeast-1': 'InstanceMigration_SG'}

    return key_lookup[region_name]


def copy_block_device_mappings(input_block_device_mappings):
    """
        Copy the block device mappings and deletes encryption attribute
    """

    output_block_device_mappings = deepcopy(input_block_device_mappings)

    for block_device in output_block_device_mappings:
        del block_device['Ebs']['Encrypted']

    return output_block_device_mappings


def create_like_instance(ec2, instance_name, app_id):
    """
        Launch a similar instance with base AMIs and instance size
        Rest of the parameters will be set to default

        The base AMI is hardcoded for now - needs to be fixed

        Output: Instance
    """

    logging.info('Launch new instance in the region %s\n',
                 args.destination_region)

    instances = ec2.create_instances(
        ImageId=get_base_ami(args.destination_region),
        MinCount=1,
        MaxCount=1,
        InstanceType='t2.micro',  # Fix for production
        KeyName=get_key_name(args.destination_region))

    instance = instances[0]

    # Wait until the instance is ready.
    logging.info('Waiting for instance %s to be running', instance.id)
    
    instance.create_tags(Tags=[
    {'Key':'Name',
     'Value':instance_name
    },
    {'Key':'Application-ID',
     'Value':app_id
    }    
    ])

    instance.wait_until_running(
        Filters=[{'Name': 'instance-id',
                  'Values': [instance.id]}])

#    waiter = ec2.get_waiter('instance_status_ok')
#    waiter.wait(InstanceIds=[instance.id])

    logging.info('Instance %s has been created\n',
                 instance.id)

    stop_instance_response = instance.stop()

    # Wait until the instance is ready.
    logging.info('Waiting for instance %s to be stopped', instance.id)

    instance.wait_until_stopped(
        Filters=[{'Name': 'instance-id',
                  'Values': [instance.id]}])

    # Instance will only contain root volume - one element
    volume_id = instance.block_device_mappings[0]['Ebs']['VolumeId']

    # Detach root volume
    detach_volume_response = instance.detach_volume(VolumeId=volume_id)

    return instance


def attach_snapshot_volume(ec2, instance, block_device, instance_name, app_id):
    """
        Creates a volume using the snapshot and attaches it to the instance
        Rest of the parameters will be set to default

        The base AMI is hardcoded for now - needs to be fixed

        Output: Instance
    """
    snapshot_id = block_device['Ebs']['SnapshotId']

    logging.info('Creating new volume for the snapshot %s\n',
                 snapshot_id)

    volume = ec2.create_volume(
        SnapshotId=snapshot_id,
        Size=block_device['Ebs']['VolumeSize'],
        VolumeType=block_device['Ebs']['VolumeType'],
        AvailabilityZone=instance.subnet.availability_zone)
        
    vol_name = ''
    if block_device['DeviceName'] == '/dev/sda1':
        vol_name = 'root'
        
    volume.create_tags(Tags=[
    {'Key':'Name',
    'Value':'{0} Volume for instance {1}'.format(vol_name,instance_name)},
    {'Key':'Application-ID',
     'Value':app_id
    }    
    ])


    # Wait for volume to be available
    while not volume.state == 'available':
        logging.info('Waiting for volume %s to be available',
                     volume.id)
        volume.load()
        time.sleep(30)

    logging.info('Volume %s has been created from snapshot: %s\n',
                 volume.id, snapshot_id)

    attach_volume_response = instance.attach_volume(
        VolumeId=volume.id,
        Device=block_device['DeviceName'])

    logging.info('Volume %s has been attached to instance %s\n',
                 volume.id, instance.id)

    return

def cleanup_intermediate_objects(ec2, object_type, object_ids):
    """
    This function cleans up the environment ,  terminates the instances
    created and deletes the snapshots , leaving only the snapshots 
    associated with the newly created AMI.
    
    Accepts: ec2 object reference , objet_type (string) , object_ids (list)
    
    Returns: void
    """
    for object_id in object_ids:
        if object_type == 'instance':
            ec2_object = ec2.Instance(object_id)
            logging.info("Terminating the instance: %s", object_id)
            ec2_object.terminate()
        if object_type == 'snapshot':
            ec2_object = ec2.Snapshot(object_id)
            logging.info("Deleting the snapshot: %s", object_id)
            ec2_object.delete()

def main():
#if __name__ == '__main__':

    # Initialize ec2 connections for sender account to source
    # and destination regions
    source_ec2 = create_connection_ec2(args.source_region,
                                       args.source_account_profile)
    source_instance = source_ec2.Instance(args.instance_id)
    source_instance_name = get_name_from_tags(source_instance.tags)
    app_id = get_app_id_from_tags(source_instance.tags)
    
    destination_ec2 = create_connection_ec2(args.destination_region,
                                            args.source_account_profile)

    # Initialize connections for receiver account to destination region
    receiver_destination_ec2 = create_connection_ec2(
        args.destination_region,
        args.destination_account_profile)

    # Step1: Make a copy of the instance
    source_image = create_image(source_ec2, args.instance_id, app_id)
#    source_image = source_ec2.Image('ami-4b2b525c')

    # Make a copy of the block device mappings - this will be used later
    destination_block_device_mappings = copy_block_device_mappings(
        source_image.block_device_mappings)

    # Step2: Make a copy of all the snapshots of the newly created AMI
    #        from source_region to destination_region
    #        using a custom encryption key (in the destination region)
    source_snapshot_ids = []
    destination_snapshot_ids = []
    owned_snapshot_ids = []
#    owned_snapshot_ids = ['snap-28eaa80f', 'snap-7dfe3c51']

    for i, device in enumerate(source_image.block_device_mappings):
        source_snapshot_id = device['Ebs']['SnapshotId']
        source_snapshot_ids.append(source_snapshot_id)
        # 2a: Make a snapshot copy from source to desination region using CMK
        destination_snapshot = copy_snapshot_from_source_dest(
            destination_ec2,
            source_snapshot_id,
            args.customer_managed_key,
            source_instance_name,
            app_id)
        destination_snapshot_ids.append(destination_snapshot.id)

        # 2b: Modify permissions on the newly created Snapshot and
        #     grant access to new account Id
        grant_access_to_receiver_account(destination_snapshot,
                                         [args.receiving_accountid])

        # 2c: Make a copy of the shared snapshot in the receiver's account
        #     using the default CMK
        owned_snapshot = copy_snapshot_default_cmk(
            receiver_destination_ec2,
            destination_snapshot.id)
        owned_snapshot_ids.append(owned_snapshot.id)
        # 2d: Update destination block device mappings with owned snapshot id
        destination_block_device_mappings[i]['Ebs']['SnapshotId'] = \
            owned_snapshot.id
#        destination_block_device_mappings[i]['Ebs']['SnapshotId'] = \
#           owned_snapshot_ids[i]

    # Step3: Launch a new instance in destination region
    # using a compatible ami i.e. same OS as the original instance. The
    # instance size should be same as the size of original instance.

    # Launch the instance, stop instance, detach root volume
    destination_instance = create_like_instance(
        receiver_destination_ec2,
        source_instance_name,
        app_id)
#       args.instance_id,
#       destination_block_device_mappings

#    destination_instance = receiver_destination_ec2.Instance('i-6dc77575')

    # Step4: Create volumes from copied snapshots and attach to the instance.
    for device in destination_block_device_mappings:

        attach_snapshot_volume(receiver_destination_ec2,
                               destination_instance,
                               device,
                               source_instance_name,
                               app_id)

    # Step5: Take an image of the instance
    destination_image = create_image(receiver_destination_ec2,
                                     destination_instance.id,
                                     app_id)
#    destination_image = receiver_destination_ec2.Image('ami-1a924e7a')
    logging.info("Final AMI %s of original instance %s from region %s has"
                 " been created under the account %s in region %s",
                 destination_image.id,
                 args.instance_id,
                 args.source_region,
                 args.receiving_accountid,
                 args.destination_region)

    # Step5: Clean up
    # 5a: Clean up destination instance
    cleanup_intermediate_objects(receiver_destination_ec2,
                                 object_type='instance',
                                 object_ids=[destination_instance.id])
    # 5b: Clean up destination snapshots - owned by receiver
    cleanup_intermediate_objects(receiver_destination_ec2,
                                 object_type='snapshot',
                                 object_ids=owned_snapshot_ids)
    # 5c: Clean up destination snapshots - owned by source
    cleanup_intermediate_objects(destination_ec2,
                                 object_type='snapshot',
                                 object_ids=destination_snapshot_ids)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("source_region", help="The source region containing "
                        "the instance to be migrated.")
    parser.add_argument("destination_region", help="The destination region "
                        "where the instance should be copied.")
    parser.add_argument("instance_id", help="AWS instance-id")
    parser.add_argument("customer_managed_key", help="ARN of the key used for "
                        "encrypting the snapshots. This key has to be shared "
                        "with the account with whom the snapshot will be "
                        "shared.")
    parser.add_argument("receiving_accountid", help="The AWS accountid where"
                        " the instance is being migrated.")
    parser.add_argument("source_account_profile", help="The profile name "
                        "containing credentials for connecting to the source "
                        "account. This profile should be available in "
                        ".aws/credentials")
    parser.add_argument("destination_account_profile", help="The profile name "
                        "containing credentials for connecting to the "
                        "destination account. This profile should be available"
                        "in .aws/credentials")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    args = parser.parse_args()

    logging.basicConfig(filename="migrate_instance.log",
                        filemode='w',
                        level=logging.INFO,
                        format='%(asctime)s %(message)s'
                       )

    main()

# Usage: python migrate_instance_b3.py 'us-east-1' 'us-west-2' 'i-042ca8945eccda376'
#         'arn:aws:kms:us-west-2:328115522647:key/08cc9cc8-df56-4ce8-8541-7fe718d65558'
#         '567973790236' 'default' 'personal'
